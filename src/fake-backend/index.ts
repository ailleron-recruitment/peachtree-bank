import {Injectable} from '@angular/core';
import {HTTP_INTERCEPTORS, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable, of, throwError} from 'rxjs';
import {delay, dematerialize, materialize, mergeMap} from 'rxjs/operators';
import transactions from './transactions.js';
import merchants from './merchant.js';

@Injectable()
export class FakeBackendInterceptor implements HttpInterceptor {

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<any> {
    const {url, method, headers, body} = request;
    const BALANCE = 5824.76;

    return of(null)
      .pipe(mergeMap(handleRoute))
      .pipe(materialize())
      .pipe(delay(500))
      .pipe(dematerialize());


    function handleRoute(): any {
      switch (true) {
        case url.endsWith('/api/transaction') && method === 'POST':
          return createTransaction();
        case url.endsWith('/api/transactions') && method === 'GET':
          return getTransactions();
        case url.endsWith('/api/merchants') && method === 'GET':
          return getMerchants();
        case url.endsWith('/api/balance') && method === 'GET':
          return getBalance();
        default:
          return next.handle(request);
      }
    }

    function createTransaction(): any {
      transactions.unshift(body);
      return ok(body);
    }

    function getTransactions(): Observable<any> {
      return ok(transactions);
    }

    function getBalance(): Observable<any> {
      return ok(BALANCE);
    }

    function getMerchants(): Observable<any> {
      return ok(merchants);
    }

    // tslint:disable-next-line:no-shadowed-variable
    function ok(body?): Observable<any> {
      return of(new HttpResponse({status: 200, body}));
    }

    function error(message): Observable<never> {
      return throwError({error: {message}});
    }
  }
}

export const fakeBackendProvider = {
  provide: HTTP_INTERCEPTORS,
  useClass: FakeBackendInterceptor,
  multi: true
};
