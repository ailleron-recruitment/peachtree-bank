import {Component, OnInit} from '@angular/core';
import {Transaction} from './model/transaction';
import {TransactionRestService} from './service/transaction-rest/transaction-rest.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  transactions: Transaction[] = [];

  constructor(private transactionService: TransactionRestService) {
  }

  receiveMessage($event): void {
    if ($event === 'GET_TRANSACTIONS') {
      this.getTransactions();
    }
  }

  getTransactions(): void {
    this.transactionService.getTransactions().subscribe(transactions => {
      this.transactions = transactions;
    });
  }

  ngOnInit(): void {
    this.getTransactions();
  }

}
