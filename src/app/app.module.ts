import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {AppComponent} from './app.component';
import {fakeBackendProvider} from '../fake-backend';
import {TransactionRestService} from './service/transaction-rest/transaction-rest.service';
import {TransactionsModule} from './module/transactions/transactions.module';
import {MatDialogModule} from '@angular/material/dialog';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    TransactionsModule,
    MatDialogModule
  ],
  providers: [
    TransactionRestService,
    fakeBackendProvider
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
