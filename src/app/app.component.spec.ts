import {TestBed} from '@angular/core/testing';
import {AppComponent} from './app.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TransactionRestService} from './service/transaction-rest/transaction-rest.service';
import {MakeTransferComponent} from './module/transactions/components/make-transfer/make-transfer.component';
import {MatDialog, MatDialogModule} from '@angular/material/dialog';
import {OverlayModule} from '@angular/cdk/overlay';
import {SearchPipe} from './module/transactions/pipes/search/search.pipe';
import {MatAutocompleteModule} from '@angular/material/autocomplete';

describe('AppComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, OverlayModule, MatDialogModule, MatAutocompleteModule],
      providers: [TransactionRestService],
      declarations: [
        MakeTransferComponent,
        AppComponent
      ],
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  // it(`should have as title 'Peachtree Bank'`, () => {
  //   const fixture = TestBed.createComponent(AppComponent);
  //   const app = fixture.componentInstance;
  //   console.log('yooooo', app.title);
  //   expect(app.title).toEqual('Peachtree Bank');
  // });

  // it('should render title', () => {
  //   const fixture = TestBed.createComponent(AppComponent);
  //   fixture.detectChanges();
  //   const compiled = fixture.nativeElement;
  //   expect(compiled.querySelector('.content span').textContent).toContain('peachtree-bank app is running!');
  // });
});
