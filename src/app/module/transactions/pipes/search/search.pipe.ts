import {Pipe, PipeTransform} from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'search',
  pure: false
})
export class SearchPipe implements PipeTransform {

  transform(items: any[], args: any[]): any {
    const searchPhrase = args[0];
    const date = args[1].date;
    const beneficiary = args[1].beneficiary;
    const amount = args[1].amount;
    let resultArray = items;

    if (date) {
      resultArray = resultArray.filter(item => date.isSame(moment(item.dates.valueDate), 'day'));
    }

    switch (beneficiary) {
      case 'ASC':
        resultArray = resultArray.sort((a, b) => a.merchant.name.localeCompare(b.merchant.name));
        break;
      case'DSC':
        resultArray = resultArray.sort((b, a) => a.merchant.name.localeCompare(b.merchant.name));
        break;
    }

    function compareDsc(a, b): number {
      if (a.transaction.amountCurrency.amount < b.transaction.amountCurrency.amount) {
        return -1;
      }
      if (a.transaction.amountCurrency.amount > b.transaction.amountCurrency.amount) {
        return 1;
      }
      return 0;
    }

    function compareAsc(a, b): number {
      if (a.transaction.amountCurrency.amount < b.transaction.amountCurrency.amount) {
        return 1;
      }
      if (a.transaction.amountCurrency.amount > b.transaction.amountCurrency.amount) {
        return -1;
      }
      return 0;
    }

    switch (amount) {
      case 'ASC':
        resultArray = resultArray.sort(compareAsc);
        break;
      case'DSC':
        resultArray = resultArray.sort(compareDsc);
        break;
    }
    if (searchPhrase) {
      resultArray = resultArray.filter(item => item.merchant.name.toLowerCase().indexOf(searchPhrase.toLowerCase()) !== -1);
    }

    return resultArray;

  }

}
