import {Component, Input} from '@angular/core';
import {Transaction} from '../../../../model/transaction';

@Component({
  selector: 'app-recent-transactions',
  templateUrl: './recent-transactions.component.html',
  styleUrls: ['./recent-transactions.component.scss']
})
export class RecentTransactionsComponent {
  searchPhrase = '';
  filter = {
    date: null,
    beneficiary: null,
    amount: null,
  };

  @Input()
  transactions: Transaction[] = [];


  setFilter(type): void {
    switch (this.filter[type]) {
      case 'ASC':
        this.filter[type] = 'DSC';
        break;
      case 'DSC':
        this.filter[type] = null;
        break;
      case null:
        this.filter[type] = 'ASC';
    }
  }

  adjustImgPath(path): string {
    return 'assets/icons/' + path.toLowerCase().replace(/\s/g, '-') + '.png';
  }

  selectDate(picker: any): void {
    if (this.filter.date) {
      this.filter.date = null;
    } else {
      picker.open();
    }
  }


}
