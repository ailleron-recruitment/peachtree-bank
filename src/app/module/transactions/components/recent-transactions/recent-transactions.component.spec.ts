import {ComponentFixture, TestBed} from '@angular/core/testing';
import {RecentTransactionsComponent} from './recent-transactions.component';
import {TransactionRestService} from '../../../../service/transaction-rest/transaction-rest.service';
import {MatDialog} from '@angular/material/dialog';
import {SearchPipe} from '../../pipes/search/search.pipe';
import {MatDatepickerModule} from '@angular/material/datepicker';

describe('RecentTransactionsComponent', () => {
  let component: RecentTransactionsComponent;
  let fixture: ComponentFixture<RecentTransactionsComponent>;
  let filter = {
    date: null,
    beneficiary: null,
    amount: null,
  };
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      providers: [TransactionRestService, MatDialog, MatDatepickerModule],
      declarations: [RecentTransactionsComponent, SearchPipe]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecentTransactionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    filter = {
      date: null,
      beneficiary: null,
      amount: null,
    };
  });

  const testFiltersArray = [
    {desc: 'ASC should change to DSC', type: 'ASC', assert: 'DSC'},
    {desc: 'DSC should change to null', type: 'DSC', assert: null},
    {desc: 'null should change to ASC', type: null, assert: 'ASC'}];

  testFiltersArray.forEach((el) => {
    it(el.desc, () => {
      component.filter.date = el.type;
      component.setFilter('date');
      expect(component.filter.date).toEqual(el.assert);
    });
  });

  it('create image path from merchant name', () => {
    expect(component.adjustImgPath('H&M online store'))
      .toEqual('assets/icons/h&m-online-store.png');
  });
});
