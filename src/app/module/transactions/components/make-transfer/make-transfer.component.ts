import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {TransactionRestService} from '../../../../service/transaction-rest/transaction-rest.service';
import {FormControl, Validators} from '@angular/forms';
import {Transaction} from '../../../../model/transaction';
import {MatDialog} from '@angular/material/dialog';
import {ConfirmDialogComponent} from '../../dialogs/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-make-transfer',
  templateUrl: './make-transfer.component.html',
  styleUrls: ['./make-transfer.component.scss']
})
export class MakeTransferComponent implements OnInit {

  constructor(private transactionService: TransactionRestService, public dialog: MatDialog) {
  }

  @Output() messageEvent = new EventEmitter<string>();

  currentAccountName = 'Free Checking(4692) - €';
  currentBalance = 0;
  merchants = null;
  merchantsFormControl = new FormControl('', Validators.required);
  amountFormControl = new FormControl('');

  openConfirmDialog(): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent,
      {
        data: {
          amount: this.amountFormControl.value,
          merchant: this.merchantsFormControl.value
        }
      }
    );

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.createTransaction();
        this.currentBalance = this.currentBalance - this.amountFormControl.value;
        this.updateAmountValidation();
      }
    });
  }

  submit(): void {
    if (!this.merchants.find(merchant => merchant === this.merchantsFormControl.value)) {
      this.merchantsFormControl.setValue('');
      return;
    }
    this.amountFormControl.markAllAsTouched();
    if (this.amountFormControl.status !== 'INVALID') {
      this.openConfirmDialog();
    }
  }

  createTransaction(): void {
    this.transactionService.post(new Transaction(this.amountFormControl.value || 0,
      this.merchantsFormControl.value)).subscribe((tra) => {
      this.messageEvent.emit('GET_TRANSACTIONS');
    });
  }

  updateAmountValidation(): void {
    this.amountFormControl.setValidators([
      Validators.required,
      Validators.min(0.01),
      Validators.max(this.currentBalance - 500)
    ]);
    this.amountFormControl.reset();
  }

  resetForm = () => {
    this.amountFormControl.setValue('');
    this.transactionService.getBalance().subscribe(balance => {
      this.currentBalance = balance;
      this.updateAmountValidation();
    });
    this.transactionService.getMerchants().subscribe(merchants => {
      this.merchants = merchants;
      this.merchantsFormControl.setValue(merchants[0]);
    });
  }

  ngOnInit(): void {
    this.resetForm();
  }
}
