import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatButtonModule} from '@angular/material/button';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatNativeDateModule, MatOptionModule} from '@angular/material/core';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {RecentTransactionsComponent} from './components/recent-transactions/recent-transactions.component';
import {MakeTransferComponent} from './components/make-transfer/make-transfer.component';
import {MatIconModule} from '@angular/material/icon';
import {SearchPipe} from './pipes/search/search.pipe';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatMomentDateModule} from '@angular/material-moment-adapter';
import {MatDialogModule} from '@angular/material/dialog';
import {ConfirmDialogComponent} from './dialogs/confirm-dialog/confirm-dialog.component';
import { MAT_DIALOG_DATA, MatDialogRef  } from '@angular/material/dialog';
@NgModule({
  declarations: [
    RecentTransactionsComponent,
    MakeTransferComponent,
    SearchPipe,
    ConfirmDialogComponent
  ],
  providers: [
    SearchPipe,
    { provide: MAT_DIALOG_DATA, useValue: {} },
    { provide: MatDialogRef , useValue: {} }
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MatAutocompleteModule,
    MatButtonModule,
    BrowserAnimationsModule,
    MatOptionModule,
    MatInputModule,
    MatFormFieldModule,
    MatIconModule,
    MatButtonToggleModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatMomentDateModule,
    MatDialogModule
  ],
  exports: [
    RecentTransactionsComponent,
    MakeTransferComponent]
})
export class TransactionsModule {
}
