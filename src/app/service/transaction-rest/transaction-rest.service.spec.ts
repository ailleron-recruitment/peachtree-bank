import {TestBed} from '@angular/core/testing';

import {TransactionRestService} from './transaction-rest.service';
import {HttpClient} from '@angular/common/http';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('RestService', () => {
  let service: TransactionRestService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [HttpClient]
    });
    service = TestBed.inject(TransactionRestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
