import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Transaction} from '../../model/transaction';
import {environment} from '@environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TransactionRestService {

  constructor(private http: HttpClient) {
  }

  getTransactions(): Observable<any[]> {
    return this.http.get<any[]>(`${environment.host}/api/transactions`);
  }

  post(transaction): Observable<Transaction> {
    return this.http.post<Transaction>(`${environment.host}/api/transaction`, transaction);
  }

  getMerchants(): Observable<any[]> {
    return this.http.get<any[]>(`${environment.host}/api/merchants`);
  }

  getBalance(): Observable<number> {
    return this.http.get<number>(`${environment.host}/api/balance`);
  }

}
