export class Transaction {

  constructor(amount, merchant) {
    this.dates.valueDate = new Date().getTime();
    this.transaction.amountCurrency.amount = amount;
    this.merchant.name = merchant;
  }

  private categoryCode = '#d51271';
  private dates = {
    valueDate: 1
  };
  private transaction = {
    amountCurrency: {
      amount: 0,
      currencyCode: 'EUR'
    },
    type: 'Salaries',
    creditDebitIndicator: 'CRDT'
  };
  private merchant = {
    name: '',
    accountNumber: 'SI64397745065188826'
  };

}
